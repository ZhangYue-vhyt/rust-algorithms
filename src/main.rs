mod sorting;

fn main() {
    let mut list1 = [1, 4, 3, 2];
    let mut list2 = [1, 4, 3, 2];
    let mut list3 = [1, 4, 3, 2];
    let mut list4 = [1, 4, 3, 2];
    sorting::selection::sort(&mut list1);
    sorting::bubble::sort(&mut list2);
    sorting::merge::sort(&mut list3);
    sorting::quick::sort(&mut list4);
    println!("{:?}", list1); // print a collection
    println!("{:?}", list2); // print a collection
    println!("{:?}", list3); // print a collection
    println!("{:?}", list4); // print a collection
}
