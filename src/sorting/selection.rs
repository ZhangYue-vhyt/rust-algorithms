// Extension method style.
pub trait Selection {
    fn selection_sort(&mut self);
}

impl<T: PartialOrd> Selection for [T] {
    fn selection_sort(&mut self) {
    let len = self.len();
    for i in 0..len {
        let mut min = i;
        for j in (i + 1)..len {
            if self[min] > self[j] {
                min = j;
            }
        }
        self.swap(i, min);
    }
    }
}

// C style.
pub fn sort<T: PartialOrd>(list: &mut [T]) {
    let len = list.len();
    for i in 0..len {
        let mut min = i;
        for j in (i + 1)..len {
            if list[min] > list[j] {
                min = j;
            }
        }
        list.swap(i, min);
    }
}
