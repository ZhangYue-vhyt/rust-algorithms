// Extension method style.
pub trait Mergesort {
    fn mergesort(&mut self);
}

impl<T: PartialOrd + Copy> Mergesort for [T] {
   fn mergesort(&mut self) {
        if self.len() > 1 {
            let mid = self.len() / 2;
            let mut left = self[..mid].to_vec();
            let mut right = self[mid..].to_vec();
            left.mergesort();
            right.mergesort();
            merge(self, &mut left, &mut right)
        }
    }
}

// C style.
pub fn sort<T: PartialOrd + Copy>(list: &mut [T]) {
    if list.len() > 1 {
        let mid = list.len() / 2;
        let mut left = list[..mid].to_vec();
        let mut right = list[mid..].to_vec();
        sort(&mut left);
        sort(&mut right);
        merge(list, &mut left, &mut right)
    }
}

fn merge<T: PartialOrd + Copy>(list: &mut [T], left: &mut [T], right: &mut [T]) {
    let (mut i, mut j, mut k) = (0, 0, 0);
    while i < left.len() && j < right.len() {
        if left[i] < right[j] {
            list[k] = left[i];
            i += 1
        } else {
            list[k] = right[j];
            j += 1;
        }
        k += 1;
    }
    while i < left.len() {
        list[k] = left[i];
        i += 1;
        k += 1;
    }
    while j < right.len() {
        list[k] = right[j];
        j += 1;
        k += 1;
    }
}

