// Extension method style.
pub trait Quicksort {
    fn quicksort(&mut self);
}

impl<T: PartialOrd + Copy> Quicksort for [T] {
    fn quicksort(&mut self) {
        if !self.is_empty() {
            helper(self, 0, self.len());
        }
    }
}

// C style.
pub fn sort<T: PartialOrd + Copy>(list: &mut [T]) {
    if !list.is_empty() {
        helper(list, 0, list.len());
    }
}

fn helper<T: PartialOrd + Copy>(list: &mut [T], low: usize, high: usize) {
    if low < high {
        let pivot = partition(list, low, high);
        helper(list, low, pivot);
        helper(list, pivot + 1, high);
    }
}

fn partition<T: PartialOrd + Copy>(list: &mut [T], low: usize, high: usize) -> usize {
    let pivot = list[low];
    let mut left = low;
    let mut right = high - 1;
    while left < right {
        while left < right && list[right] >= pivot {
            right -= 1;
        }
        while left < right && list[left] <= pivot {
            left += 1;
        }
        list.swap(left, right);
    }
    list.swap(low, left);
    left
}

/// # Annotations
/// - `#[allow(dead_code)]`: Allow unused code.
/// - `#[deprecated]`: Throw a warning if this function is used.
/// - [The Rust Reference](https://doc.rust-lang.org/reference/attributes.html)
#[allow(dead_code)]
#[deprecated(note = "This function works but I don't understand how it works.")]
fn another_partition<T: PartialOrd + Copy>(list: &mut [T], low: usize, high: usize) -> usize {
    let pivot = list[low];
    let mut j = low;
    for i in low..high {
        if list[i] < pivot {
            j += 1;
            list.swap(i, j);
        }
    }
    list.swap(j, low);
    j
}
