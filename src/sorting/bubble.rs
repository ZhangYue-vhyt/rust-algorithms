// Extension method style.
pub trait Bubble {
    fn bubble_sort(&mut self);
}

impl<T: PartialOrd> Bubble for [T] {
    fn bubble_sort(&mut self) {
        let len = self.len();
        for i in 0..len {
            let mut swap_count = 0;
            for j in 1..(len - i) {
                if self[j - 1] > self[j] {
                    self.swap(j - 1, j);
                    swap_count += 1;
                }
            }
            if swap_count == 0 {
                break;
            }
        }
    }
}

// C style.
pub fn sort<T: PartialOrd>(list: &mut [T]) {
    let len = list.len();
    for i in 0..len {
        let mut swap_count = 0;
        for j in 1..(len - i) {
            if list[j - 1] > list[j] {
                list.swap(j - 1, j);
                swap_count += 1;
            }
        }
        if swap_count == 0 {
            break;
        }
    }
}
